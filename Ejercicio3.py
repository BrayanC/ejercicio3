# Escribe un programa para pedirle al usuario el número de las horas
# y la tarifa por hora para calcular el salario bruto.


# Autor: Brayan Gonzalo Cabrera Cabrera.

# brayan.cabrera@unl.edu.ec


Horas= int(input("Escribe el número de Horas: "))
Tarifa= float(input("Escribe la tarifa por hora: "))

Salario= Horas * Tarifa

print("El salario bruto a cobrar es de: ", Salario)

